# CajaRegistradora

## Prueba backend Merqueo

### Servicio REST Caja registradora

<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg"  alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg"  alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg"  alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg"  alt="License"></a>
</p>

# Instalación y ejecución

- Clonar el proyecto con la siguiente línea de comando:
  `git clone https://gitlab.com/violamentes34/cajaregistradora.git`
- Crear archivo `.env` copiando el archivo `.env.example`, en el archivo editar la configuración de base de datos.
- Instalar las dependencias con el siguiente comando:
  `composer install`
- Ejecutar las migraciones en base de datos con el siguiente comando:
  `php artisan migrate`
- Para levantar el servicio, ejecutar el siguiente comando:
  `php artisan migrate`

# Modelos

Existen dos modelos: CashRegister y Logs, estos modelos se relacionan de la siguiente manera:

- Cada caja registradora puede tener uno o muchos registros de logs.
- Cada log está asociado a una única caja registradora.
- Cada log tiene multiples registros de entrada y/o salida que pertenecen a la caja registradora asociada.

# Endpoints

Para los endpoints se deben en cuenta el nombre de los siguientes campos según se requiera en el endpoints a utilizar:

## **Campos monedas/billetes**

- **coin50:** monedas de $ 50
- **coin100:** monedas de $ 100
- **coin200:** monedas de $ 200
- **coin500:** monedas de $ 500
- **bill1k:** billetes de $ 1.000
- **bill2k:** billetes de $ 2.000
- **bill5k:** billetes de $ 5.000
- **bill10k:** billetes de $ 10.000
- **bill20k:** billetes de $ 20.000
- **bill50k:** billetes de $ 50.000
- **bill100k:** billetes de $ 100.000

Los end creados para el ejercicio son los siguientes:

### **Cargar caja registradora**

**`POST`** | `http://127.0.0.1:8000/api/caja/cargar/`  
`Tener en cuenta los Campos monedas/billetes `

Se debe especificar en el cuerpo de la petición las monedas/billetes que se van a cargar a la caja registradora e
indicar un nombre para la caja **(register).**

Al cargar dinero nuevamente para la misma caja registradora, se va a acumulando el saldo en la caja registradora.

![](resources\img\cargarCajaParam.JPG)

![](resources\img\cargarCajaResult.JPG)


### **Vaciar caja registradora**

**`POST`** | `http://127.0.0.1:8000/api/caja/vaciar/`  
`Requerido campo id: id de la caja registradora. | id=1`

Se debe especificar en el cuerpo de la petición el id de la caja registradora que se va a dejar todo el saldo en cero.

![](resources\img\vaciarCajaParam.JPG)

![](resources\img\vaciarCajaResult.JPG)


### **Estado caja registradora**

**`GET`** | `http://127.0.0.1:8000/api/caja/estado/`  
`Requerido campo id: id de la caja registradora. | id=1`

Se debe especificar en los parámetros de la petición el id de la caja registradora a consultar.

![](resources\img\estadoCajaParam.JPG)

![](resources\img\estadoCajaResult.JPG)


### **Realizar pago**

**`POST`** | `http://127.0.0.1:8000/api/pagos/hacer/`  
`Tener en cuenta los Campos monedas/billetes que se van a entregar`  
`Requerido campo id: id de la caja registradora. | id:1`  
`Requerido campo fullpayment: total de la compra a pagar. | fullpayment:130000`

Se debe especificar en el cuerpo de la petición el id de la caja registradora a la que se le va a entregar el dinero,
las monedas/billetes que se van a entregar y el total de la compra a pagar.

![](resources\img\realizarPagoParam.JPG)

![](resources\img\realizarPagoResult.JPG)


### **Ver logs**

**`GET`** | `http://127.0.0.1:8000/api/log/ver/`  
`Requerido `
`campo cashregister_id: id de la caja registradora que se le generó log. | cashregister_id=1`

Se debe especificar en los parámetros de la petición el id de la caja registradora para consultar los logs generados.

![](resources\img\verLogParam.JPG)

![](resources\img\verLogResult.JPG)


# **Modelo relacional**

![](resources\img\modeloRelacional.JPG)

