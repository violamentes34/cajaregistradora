<?php

namespace App\Http\Api\Controllers\CashRegister;

use App\Interfaces\UsesCases\EmptyCashRegisterInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response as FacadeResponse;

/**
 * Class EmptyController
 * @package App\Http\Api\Controllers\CashRegister
 */
class EmptyController
{
    /**
     * @var EmptyCashRegisterInterface
     */
    private $emptyCashRegister;

    /**
     * EmptyController constructor.
     * @param EmptyCashRegisterInterface $emptyCashRegister
     */
    public function __construct(EmptyCashRegisterInterface $emptyCashRegister)
    {
        $this->emptyCashRegister = $emptyCashRegister;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        return FacadeResponse::json($this->emptyCashRegister->execute($request), 200);
    }
}
