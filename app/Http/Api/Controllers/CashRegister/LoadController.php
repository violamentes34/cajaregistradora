<?php

namespace App\Http\Api\Controllers\CashRegister;

use App\Interfaces\UsesCases\LoadCashRegisterInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response as FacadeResponse;

/**
 * Class LoadController
 * @package App\Http\Api\Controllers\CashRegister
 */
class LoadController
{
    /**
     * @var LoadCashRegisterInterface
     */
    private $loadCashRegister;

    /**
     * LoadController constructor.
     * @param LoadCashRegisterInterface $loadCashRegister
     */
    public function __construct(LoadCashRegisterInterface $loadCashRegister)
    {
        $this->loadCashRegister = $loadCashRegister;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        return FacadeResponse::json($this->loadCashRegister->execute($request), 200);
    }
}
