<?php

namespace App\Http\Api\Controllers\CashRegister;

use App\Interfaces\UsesCases\StatusCashRegisterInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response as FacadeResponse;

/**
 * Class StatusController
 * @package App\Http\Api\Controllers\CashRegister
 */
class StatusController
{
    /**
     * @var StatusCashRegisterInterface
     */
    private $statusCashRegister;

    /**
     * StatusController constructor.
     * @param StatusCashRegisterInterface $statusCashRegister
     */
    public function __construct(StatusCashRegisterInterface $statusCashRegister)
    {
        $this->statusCashRegister = $statusCashRegister;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        return FacadeResponse::json($this->statusCashRegister->execute($request), 200);
    }
}
