<?php

namespace App\Http\Api\Controllers\CashRegister;

use App\Interfaces\Repositories\CashRegisterInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response as FacadeResponse;

/**
 * Class ViewController
 * @package App\Http\Api\Controllers\CashRegister
 */
class ViewController
{
    /**
     * @var CashRegisterInterface
     */
    private $cashRegister;

    /**
     * CrudController constructor.
     * @param CashRegisterInterface $cashRegister
     */
    public function __construct(CashRegisterInterface $cashRegister)
    {
        $this->cashRegister = $cashRegister;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return FacadeResponse::json($this->cashRegister->read(), 200);
    }
}
