<?php

namespace App\Http\Api\Controllers\Log;

use App\Interfaces\UsesCases\ViewLogInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response as FacadeResponse;

/**
 * Class ViewController
 * @package App\Http\Api\Controllers\Log
 */
class ViewController
{
    /**
     * @var ViewLogInterface
     */
    private $viewLog;

    /**
     * ViewController constructor.
     * @param ViewLogInterface $viewLog
     */
    public function __construct(ViewLogInterface $viewLog)
    {
        $this->viewLog = $viewLog;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        return FacadeResponse::json($this->viewLog->execute($request), 200);
    }
}
