<?php

namespace App\Http\Api\Controllers\Payment;

use App\Interfaces\UsesCases\MakePaymentInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response as FacadeResponse;

/**
 * Class MakeController
 * @package App\Http\Api\Controllers\Payment
 */
class MakeController
{
    /**
     * @var MakePaymentInterface
     */
    private $payment;

    /**
     * MakeController constructor.
     * @param MakePaymentInterface $payment
     */
    public function __construct(MakePaymentInterface $payment)
    {
        $this->payment = $payment;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        return FacadeResponse::json($this->payment->execute($request), 200);
    }
}
