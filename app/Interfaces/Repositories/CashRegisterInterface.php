<?php

namespace App\Interfaces\Repositories;

use App\Models\CashRegister;
use Illuminate\Http\Request;

/**
 * Interface CashRegisterInterface
 * @package App\Interfaces\Repositories
 */
interface CashRegisterInterface
{
    /**
     * @param array $cashRegister
     * @return mixed
     */
    public function load(array $cashRegister);

    /**
     * @param Request $request
     * @return mixed
     */
    public function emptyCash(Request $request);

    /**
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request);

    /**
     * @return array
     */
    public function read(): array;

    /**
     * @param CashRegister $cashRegited
     * @return mixed
     */
    public function update(CashRegister $cashRegited);

    /**
     * @param Request $request
     * @return bool
     */
    public function delete(Request $request): bool;

    /**
     * @param Request $request
     * @return mixed
     */
    public function readById(Request $request);

    /**
     * @param Request $request
     * @return mixed
     */
    public function existsByRegister(Request $request);

    /**
     * @param Request $request
     * @return mixed
     */
    public function existsById(Request $request);

    /**
     * @param Request $request
     * @return mixed
     */
    public function existsByIdAndRegister(Request $request);
}
