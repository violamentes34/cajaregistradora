<?php

namespace App\Interfaces\Repositories;

use App\Models\CashRegister;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

/**
 * Interface LogInterface
 * @package App\Interfaces\Repositories
 */
interface LogInterface
{
    /**
     * @param array $datos
     * @return Collection
     */
    public function load(array $datos): Collection;

    /**
     * @param CashRegister $cashiered
     * @return mixed
     */
    public function create(Cashregister $cashiered);

    /**
     * @return array
     */
    public function read(): array;

    /**
     * @param Request $request
     * @return bool
     */
    public function update(Request $request): bool;

    /**
     * @param Request $request
     * @return bool
     */
    public function delete(Request $request): bool;

    /**
     * @param Request $request
     * @return mixed
     */
    public function existsById(Request $request);

    /**
     * @param Request $request
     * @return mixed
     */
    public function existsByIdCashRegister(Request $request);
}
