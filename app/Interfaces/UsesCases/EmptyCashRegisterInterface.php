<?php

namespace App\Interfaces\UsesCases;

use Illuminate\Http\Request;

/**
 * Interface EmptyCashRegisterInterface
 * @package App\Interfaces\UsesCases
 */
interface EmptyCashRegisterInterface
{
    /**
     * @param Request $request
     * @return bool
     */
    public function execute(Request $request): bool;
}
