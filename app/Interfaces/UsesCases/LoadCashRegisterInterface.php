<?php

namespace App\Interfaces\UsesCases;

use Illuminate\Http\Request;

/**
 * Interface LoadCashRegisterInterface
 * @package App\Interfaces\UsesCases
 */
interface LoadCashRegisterInterface
{
    /**
     * @param Request $request
     * @return bool
     */
    public function execute(Request $request): bool;
}
