<?php

namespace App\Interfaces\UsesCases;

use Illuminate\Http\Request;

/**
 * Interface MakePaymentInterface
 * @package App\Interfaces\UsesCases
 */
interface MakePaymentInterface
{

    /**
     * @param Request $request
     * @return mixed
     */
    public function execute(Request $request);
}
