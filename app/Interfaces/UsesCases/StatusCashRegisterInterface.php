<?php

namespace App\Interfaces\UsesCases;

use Illuminate\Http\Request;

/**
 * Interface StatusCashRegisterInterface
 * @package App\Interfaces\UsesCases
 */
interface StatusCashRegisterInterface
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function execute(Request $request);
}
