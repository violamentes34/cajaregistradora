<?php

namespace App\Interfaces\UsesCases;

use Illuminate\Http\Request;

/**
 * Interface ViewLogInterface
 * @package App\Interfaces\UsesCases
 */
interface ViewLogInterface
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function execute(Request $request);
}
