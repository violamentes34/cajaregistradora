<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Log
 * @package App\Models
 */
class Log extends Model
{
    use HasFactory;

    protected $table = 'logs';

    protected $fillable = [
        'coin50', 'coin100', 'coin200', 'coin500', 'bill1k', 'bill2k', 'bill5k', 'bill10k', 'bill20k', 'bill50k', 'bill100k', 'cashregister_id'
    ];
}
