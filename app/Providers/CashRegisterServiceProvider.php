<?php

namespace App\Providers;

use App\Interfaces\Repositories\CashRegisterInterface;
use App\Interfaces\Repositories\LogInterface;
use App\Interfaces\UsesCases\EmptyCashRegisterInterface;
use App\Interfaces\UsesCases\LoadCashRegisterInterface;
use App\Interfaces\UsesCases\MakePaymentInterface;
use App\Interfaces\UsesCases\StatusCashRegisterInterface;
use App\Interfaces\UsesCases\ViewLogInterface;
use App\Repositories\CashRegisterRepository;
use App\Repositories\LogRepository;
use App\UsesCases\CashRegister\EmptyCashRegisterUseCase;
use App\UsesCases\CashRegister\LoadCashRegisterUseCase;
use App\UsesCases\CashRegister\StatusCashRegisterUseCase;
use App\UsesCases\Log\ViewLogUseCase;
use App\UsesCases\Payment\MakePaymentUseCase;
use Illuminate\Support\ServiceProvider;

/**
 * Class CashRegisterServiceProvider
 * @package App\Providers
 */
class CashRegisterServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CashRegisterInterface::class, CashRegisterRepository::class);
        $this->app->bind(LogInterface::class, LogRepository::class);
        $this->app->bind(EmptyCashRegisterInterface::class, EmptyCashRegisterUseCase::class);
        $this->app->bind(LoadCashRegisterInterface::class, LoadCashRegisterUseCase::class);
        $this->app->bind(StatusCashRegisterInterface::class, StatusCashRegisterUseCase::class);
        $this->app->bind(ViewLogInterface::class, ViewLogUseCase::class);
        $this->app->bind(MakePaymentInterface::class, MakePaymentUseCase::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
