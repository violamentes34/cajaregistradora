<?php

namespace App\Repositories;

use App\Interfaces\Repositories\CashRegisterInterface;
use App\Models\CashRegister;
use Illuminate\Http\Request;

/**
 * Class CashRegisterRepository
 * @package App\Repositories
 */
class CashRegisterRepository implements CashRegisterInterface
{
    /**
     * @param array $cashRegister
     * @return bool
     */
    public function load(array $cashRegister): bool
    {
        return true;
    }

    /**
     * @param Request $request
     * @return bool|mixed
     */
    public function emptyCash(Request $request)
    {
        $cashiered = CashRegister::where('id', $request->id)->first();
        if (!$cashiered) {
            return false;
        }
        $cashiered->coin50 = 0;
        $cashiered->coin100 = 0;
        $cashiered->coin200 = 0;
        $cashiered->coin500 = 0;
        $cashiered->bill1k = 0;
        $cashiered->bill2k = 0;
        $cashiered->bill5k = 0;
        $cashiered->bill10k = 0;
        $cashiered->bill20k = 0;
        $cashiered->bill50k = 0;
        $cashiered->bill100k = 0;
        $cashiered->save();
        return $this->readById($cashiered->id);
    }

    /**
     * Crear registro
     * Retorna la tupla del registro creado
     *
     * @param Request $request
     * @return false|mixed
     */
    public function create(Request $request)
    {
        $array = $this->validateZeros($request->toArray());
        if (empty(array_filter($array))) {
            return false;
        }
        if ($response = CashRegister::create($array)) {
            return $this->readById($response->id);
        }
    }

    /**
     * Lectura a toda la tabla
     *
     * @return array
     */
    public function read(): array
    {
        return CashRegister::all()->toArray();
    }

    /**
     *
     * Si llega el campo pero sin valor, se actualiza a cero
     * Si no llega el campo, no se actualiza
     *
     * Retorna la tupla del registro actualizado
     *
     * @param CashRegister $cashRegited
     * @return bool|mixed
     */
    public function update(CashRegister $cashRegited)
    {
        if (CashRegister::find($cashRegited->id)->update($cashRegited->toArray())) {
            return $this->readById($cashRegited->id);
        }
    }

    /**
     * Se eliminan todos los id separados por coma que lleguen en el request
     *
     * @param Request $request
     * @return bool
     */
    public function delete(Request $request): bool
    {
        $ids = explode(',', $request->toArray()['id']);
        return CashRegister::destroy($ids);
    }

    /**
     * Si todos los valores del array son cero, retorna false
     *
     * @param array $array
     * @return array
     */
    public function validateZeros(array $array): array
    {
        foreach ($array as $data) {
            if (is_null($data)) $array[array_search($data, $array)] = '0';
        }
        return $array;
    }

    /**
     * Búsqueda de un registro por id
     *
     * @param Request $id
     * @return mixed
     */
    public function readById($id)
    {
        return CashRegister::find($id);
    }

    /**
     * Valida existencia de registro por campo register
     *
     * @param Request $request
     * @return mixed
     */
    public function existsByRegister(Request $request)
    {
        return CashRegister::where('register', $request->register)->first();
    }

    /**
     * Valida existencia de registro por id
     *
     * @param Request $request
     * @return mixed
     */
    public function existsById(Request $request)
    {
        return CashRegister::where('id', $request->id)->first();
    }

    public function existsByIdAndRegister(Request $request)
    {
        return CashRegister::select('coin50', 'coin100', 'coin200', 'coin500', 'bill1k', 'bill2k', 'bill5k', 'bill10k', 'bill20k', 'bill50k', 'bill100k')
            ->where('id', $request->id)
            ->where('register', $request->register)->first();
    }
}
