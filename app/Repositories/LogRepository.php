<?php

namespace App\Repositories;

use App\Interfaces\Repositories\LogInterface;
use App\Models\CashRegister;
use App\Models\Log;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

/**
 * Class LogRepository
 * @package App\Repositories
 */
class LogRepository implements LogInterface
{
    /**
     * @param array $datos
     * @return Collection
     */
    public function load(array $datos): Collection
    {
        return Log::where('fecha')->get()->toArray();
    }

    /**
     * @param CashRegister $cashiered
     * @return bool
     */
    public function create(CashRegister $cashiered)
    {
        $array = $cashiered->toArray();
        $log = new Log;
        $log->coin50 = $array['coin50'];
        $log->coin100 = $array['coin100'];
        $log->coin200 = $array['coin200'];
        $log->coin500 = $array['coin500'];
        $log->bill1k = $array['bill1k'];
        $log->bill2k = $array['bill2k'];
        $log->bill5k = $array['bill5k'];
        $log->bill10k = $array['bill10k'];
        $log->bill20k = $array['bill20k'];
        $log->bill50k = $array['bill50k'];
        $log->bill100k = $array['bill100k'];
        $log->cashregister_id = $array['id'];
        if ($response = Log::create($log->toArray())) {
            return $this->readById($response->id);
        }
    }

    /**
     * @return array
     */
    public function read(): array
    {
        return Log::all()->toArray();
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function update(Request $request): bool
    {
        $log = Log::where('id', $request->id)->first();
        if (!$log) {
            return false;
        }
        $log->coin50 = $request->coin50;
        $log->coin100 = $request->coin100;
        $log->coin200 = $request->coin200;
        $log->coin500 = $request->coin500;
        $log->bill1k = $request->bill1k;
        $log->bill2k = $request->bill2k;
        $log->bill5k = $request->bill5k;
        $log->bill10k = $request->bill10k;
        $log->bill20k = $request->bill20k;
        $log->bill50k = $request->bill50k;
        $log->bill100k = $request->bill100k;
        $log->cashregister_id = $request->cashregister_id;
        return $log->save();
    }

    /**
     * @param $id
     * @return bool
     */
    public function delete($id): bool
    {
        return Log::destroy($id);
    }


    /**
     * Búsqueda de un registro por id
     *
     * @param $id
     * @return mixed
     */
    public function readById($id)
    {
        return Log::find($id);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function existsById(Request $request)
    {
        return Log::where('id', $request->id)->first();
    }

    /**
     * @param Request $request
     * @return mixed|void
     */
    public function existsByIdCashRegister(Request $request)
    {
        return Log::where('cashregister_id', $request->cashregister_id)->get();
    }
}
