<?php

namespace App\UsesCases\CashRegister;

use App\Interfaces\Repositories\CashRegisterInterface;
use App\Interfaces\Repositories\LogInterface;
use App\Interfaces\UsesCases\EmptyCashRegisterInterface;
use Illuminate\Http\Request;

/**
 * Class EmptyCashRegisterUseCase
 * @package App\UsesCases\CashRegister
 */
class EmptyCashRegisterUseCase implements EmptyCashRegisterInterface
{
    /**
     * @var CashRegisterInterface
     */
    private $cashRegister;

    /**
     * @var LogInterface
     */
    private $log;

    /**
     * EmptyCashRegisterUseCase constructor.
     * @param CashRegisterInterface $cashRegister
     * @param LogInterface $log
     */
    public function __construct(
        CashRegisterInterface $cashRegister,
        LogInterface $log
    ) {
        $this->cashRegister = $cashRegister;
        $this->log = $log;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function execute(Request $request): bool
    {
        $emptyCash = $this->cashRegister->existsById($request);
        if ($emptyCash) {
            $arrayBefore = $this->cashRegister->validateZeros($emptyCash->toArray());
            foreach ($arrayBefore as $data) {
                $key = array_search($data, $arrayBefore);
                if (intval($arrayBefore[$key]) <> 0 and $key <> 'id') {
                    $emptyCash->$key = 0;
                }
            }
            $emptyCash = $this->cashRegister->update($emptyCash);
            foreach ($arrayBefore as $data) {
                $key = array_search($data, $arrayBefore);
                $emptyCash->$key = intval($arrayBefore[$key]);
            }
        }
        $this->log->create($emptyCash);
        return true;
    }
}
