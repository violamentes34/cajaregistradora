<?php

namespace App\UsesCases\CashRegister;

use App\Interfaces\Repositories\CashRegisterInterface;
use App\Interfaces\Repositories\LogInterface;
use App\Interfaces\UsesCases\LoadCashRegisterInterface;
use Illuminate\Http\Request;

/**
 * Class LoadCashRegisterUseCase
 * @package App\UsesCases\CashRegister
 */
class LoadCashRegisterUseCase implements LoadCashRegisterInterface
{
    /**
     * @var CashRegisterInterface
     */
    private $cashRegister;

    /**
     * @var LogInterface
     */
    private $log;

    /**
     * LoadCashRegisterUseCase constructor.
     * @param CashRegisterInterface $cashRegister
     * @param LogInterface $log
     */
    public function __construct(
        CashRegisterInterface $cashRegister,
        LogInterface $log
    )
    {
        $this->cashRegister = $cashRegister;
        $this->log = $log;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function execute(Request $request): bool
    {
        $loadCash = $this->cashRegister->existsByRegister($request);
        if ($loadCash) {
            $arrayBefore = $this->cashRegister->validateZeros($request->toArray());
            $cashRegited = $this->cashRegister->existsByRegister($request);
            foreach ($arrayBefore as $data) {
                $key = array_search($data, $arrayBefore);
                if (intval($arrayBefore[$key])) {
                    $cashRegited->$key += intval($arrayBefore[$key]);
                }
            }
            $cashRegited = $this->cashRegister->update($cashRegited);
            foreach ($arrayBefore as $data) {
                $key = array_search($data, $arrayBefore);
                if (intval($arrayBefore[$key])) {
                    $cashRegited->$key = intval($arrayBefore[$key]);
                }
            }
        } else {
            if (empty($request->register)) {
                return false;
            } else {
                $cashRegited = $this->cashRegister->create($request);
            }
        }
        $this->log->create($cashRegited);
        return true;
    }
}
