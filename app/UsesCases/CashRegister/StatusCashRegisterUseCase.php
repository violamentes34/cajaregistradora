<?php

namespace App\UsesCases\CashRegister;

use App\Interfaces\Repositories\CashRegisterInterface;
use App\Interfaces\UsesCases\StatusCashRegisterInterface;
use Illuminate\Http\Request;

class StatusCashRegisterUseCase implements StatusCashRegisterInterface
{
    /**
     * @var CashRegisterInterface
     */
    private $cashRegister;

    /**
     * StatusCashRegisterUseCase constructor.
     * @param CashRegisterInterface $cashRegister
     */
    public function __construct(CashRegisterInterface $cashRegister)
    {
        $this->cashRegister = $cashRegister;
    }

    /**
     * @param Request $request
     * @return false|mixed
     */
    public function execute(Request $request)
    {
        $statusCash = $this->cashRegister->existsById($request);
        return ($statusCash) ? $statusCash : false;
    }
}
