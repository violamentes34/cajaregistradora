<?php

namespace App\UsesCases\Log;

use App\Interfaces\Repositories\LogInterface;
use App\Interfaces\UsesCases\ViewLogInterface;
use Illuminate\Http\Request;

/**
 * Class ViewLogUseCase
 * @package App\UsesCases\Log
 */
class ViewLogUseCase implements ViewLogInterface
{
    /**
     * @var LogInterface
     */
    private $log;

    /**
     * ViewLogUseCase constructor.
     * @param LogInterface $log
     */
    public function __construct(LogInterface $log)
    {
        $this->log = $log;
    }

    /**
     * @param Request $request
     * @return bool|mixed
     */
    public function execute(Request $request)
    {
        $viewLog = $this->log->existsByIdCashRegister($request);
        return ($viewLog) ? $viewLog : false;
    }
}
