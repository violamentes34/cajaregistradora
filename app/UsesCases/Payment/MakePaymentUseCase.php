<?php

namespace App\UsesCases\Payment;

use App\Interfaces\Repositories\CashRegisterInterface;
use App\Interfaces\Repositories\LogInterface;
use App\Interfaces\UsesCases\MakePaymentInterface;
use Illuminate\Http\Request;

/**
 * Class MakePaymentUseCase
 * @package App\UsesCases\Payment
 */
class MakePaymentUseCase implements MakePaymentInterface
{
    /**
     * @var CashRegisterInterface
     */
    private $cashRegister;

    /**
     * @var LogInterface
     */
    private $log;

    /**
     * @var array
     */
    private $cash;

    /**
     * LoadCashRegisterUseCase constructor.
     * @param CashRegisterInterface $cashRegister
     * @param LogInterface $log
     */
    public function __construct(
        CashRegisterInterface $cashRegister,
        LogInterface $log
    ) {
        $this->cashRegister = $cashRegister;
        $this->log = $log;
        $this->cash = array(
            'coin50' => 50,
            'coin100' => 100,
            'coin200' => 200,
            'coin500' => 500,
            'bill1k' => 1000,
            'bill2k' => 2000,
            'bill5k' => 5000,
            'bill10k' => 10000,
            'bill20k' => 20000,
            'bill50k' => 50000,
            'bill100k' => 100000
        );
    }

    /**
     * @param Request $request
     * @return bool|mixed
     */
    public function execute(Request $request)
    {
        $paymentArray = $request->toArray();
        $paymentValid = $this->calculatePayment($paymentArray);
        if ($this->verifyPayment($paymentValid[0], $paymentArray['fullpayment'])) {
            $change = $this->calculateChange($paymentValid[0], $paymentArray['fullpayment']);
            $cashRegister = $this->cashRegister->existsByIdAndRegister($request)->toArray();
            asort($cashRegister, true);
//            dd($cashRegister);
        }
        return true;
//        $viewLog = $this->log->existsByIdCashRegister($request);
//        return ($viewLog) ? $viewLog : false;
    }

    /**
     * @param array $payment
     * @return array
     */
    public function calculatePayment(array $payment): array
    {
        $paymentValid = [];
        foreach ($payment as $data) {
            $key = array_search($data, $payment);
            foreach ($this->cash as $cash) {
                $keyCash = array_search($cash, $this->cash);
                if ($key == $keyCash) {
                    $paymentValid[$key] = intval($this->cash[$key]) * intval($data);
                    break;
                }
            }
        }
        $fullpayment = 0;
        foreach ($paymentValid as $data) {
            $fullpayment += $data;
        }
        array_push($paymentValid, $fullpayment);
        return $paymentValid;
    }


    /**
     * @param int $sumPayment
     * @param int $fullPayment
     * @return bool
     */
    public function verifyPayment(int $sumPayment, int $fullPayment): bool
    {
        return $sumPayment >= $fullPayment;
    }

    /**
     * @param int $sumPayment
     * @param int $fullPayment
     * @return int
     */
    public function calculateChange(int $sumPayment, int $fullPayment)
    {
        return $sumPayment - $fullPayment;
    }
}
