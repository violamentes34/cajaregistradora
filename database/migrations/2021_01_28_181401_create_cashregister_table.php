<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCashregisterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cashregister', function (Blueprint $table) {
            $table->id();
            $table->string('register')->nullable(false);
            $table->integer('coin50')->nullable(true)->default(0);
            $table->integer('coin100')->nullable(true)->default(0);
            $table->integer('coin200')->nullable(true)->default(0);
            $table->integer('coin500')->nullable(true)->default(0);
            $table->integer('bill1k')->nullable(true)->default(0);
            $table->integer('bill2k')->nullable(true)->default(0);
            $table->integer('bill5k')->nullable(true)->default(0);
            $table->integer('bill10k')->nullable(true)->default(0);
            $table->integer('bill20k')->nullable(true)->default(0);
            $table->integer('bill50k')->nullable(true)->default(0);
            $table->integer('bill100k')->nullable(true)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cashregister');
    }
}
