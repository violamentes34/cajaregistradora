<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->id();
            $table->integer('coin50')->default(0);
            $table->integer('coin100')->default(0);
            $table->integer('coin200')->default(0);
            $table->integer('coin500')->default(0);
            $table->integer('bill1k')->default(0);
            $table->integer('bill2k')->default(0);
            $table->integer('bill5k')->default(0);
            $table->integer('bill10k')->default(0);
            $table->integer('bill20k')->default(0);
            $table->integer('bill50k')->default(0);
            $table->integer('bill100k')->default(0);
            $table->unsignedBigInteger('cashregister_id');
            $table->foreign('cashregister_id')->references('id')->on('cashregister');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
