<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/**
 * Routes for cash register
 */
Route::group(['prefix' => 'caja/'], function () {
    Route::post('cargar/', [
        'uses' => 'App\Http\Api\Controllers\CashRegister\LoadController@index'
    ]);
    Route::post('vaciar/', [
        'uses' => 'App\Http\Api\Controllers\CashRegister\EmptyController@index'
    ]);
    Route::get('estado/', [
        'uses' => 'App\Http\Api\Controllers\CashRegister\StatusController@index'
    ]);
});

/**
 * Routes for logs
 */
Route::group(['prefix' => 'log/'], function () {
    Route::get('ver/', [
        'uses' => 'App\Http\Api\Controllers\Log\ViewController@index'
    ]);
});

/**
 * Routes for payments
 */
Route::group(['prefix' => 'pagos/'], function () {
    Route::post('hacer/', [
        'uses' => 'App\Http\Api\Controllers\Payment\MakeController@index'
    ]);
});
